package bri;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;


public class Serveur implements Runnable {
	private ServerSocket s;
	private Class<? extends Runnable> service;
	
	
	public Serveur(int port, Class<? extends Runnable> service) throws IOException {
		this.s = new ServerSocket(port);
		try {
			Class.forName(service.getName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		this.service = service;
	}

	@Override
	public void run() {
		try {
			while(true) {
					//cr�ation du service donn� en param�tre
					new Thread(service.getConstructor(Socket.class).newInstance(s.accept())).start();
			}
		} catch (Exception e) {
			System.out.println("Le service " + service.getSimpleName() + " n'a pas pu �tre lanc�");
		}
		finally {
			System.out.println("Fermeture du service " + service.getSimpleName());
			try {
				s.close();
			} catch (IOException e) {}
		}
	}
}
