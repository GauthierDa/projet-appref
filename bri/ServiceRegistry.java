package bri;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import Exception.ServiceMalFormé;
import Utilisateur.Programmeur;


public class ServiceRegistry {
private static Map<String, Programmeur> listProgrammeur = new ConcurrentHashMap<String, Programmeur>();
	
	static {
		init();
	}
	
	//vérification reflexive du service et ajout si tous les critères sont respectés
	public static void addService(Class<?> classe) throws ServiceMalFormé {
		if(!implementService(classe))
			throw new ServiceMalFormé("Le service n'implémente pas l'interface Service");
		
		if(Modifier.isAbstract(classe.getModifiers()))
			throw new ServiceMalFormé("Le service ne doit pas être abstrait");
		
		if(!Modifier.isPublic(classe.getModifiers()))
			throw new ServiceMalFormé("Le service doit être public");
		
		if(!constructeurSocket(classe))
			throw new ServiceMalFormé("Le service n'a pas de constructeur avec une socket");
		
		if(!attributSocket(classe))
			throw new ServiceMalFormé("Le service n'a pas d'attribut socket");

		if(!methodeStringue(classe))
			throw new ServiceMalFormé("Le service n'a pas de méthode correcte toStringue");
	}
		
	
	private static boolean implementService(Class<?> classe) {
		Class<?>[] interfaces = classe.getInterfaces();
		for(Class<?> c : interfaces) {
			if(c.getSimpleName().equals("Service")) {
				return true;
			}			
		}
		return false;
	}
	
	private static boolean constructeurSocket(Class<?> c) {
		try {
			c.getConstructor(Socket.class);
			return true;
		} catch (NoSuchMethodException| SecurityException e) {
			return false;
		}
	}
	
	private static boolean attributSocket(Class<?> c) {
		Field[] fields = c.getDeclaredFields();
		for(Field f : fields) {
			if(f.getType().equals(Socket.class) 
					&& Modifier.isPrivate(f.getModifiers())
					&& Modifier.isFinal(f.getModifiers()))
				return true;
		}
		return false;
	}
	
	private static boolean methodeStringue(Class<?> c) {
		Method[] methods = c.getMethods();
		for(Method m : methods) {
			if(m.getName().equals("toStringue") 
					&& Modifier.isPublic(m.getModifiers()) 
					&& Modifier.isStatic(m.getModifiers()) 
					&& m.getParameterCount()==0
					&& m.getAnnotatedExceptionTypes().length == 0);
			return true;
		}
		return false;
	}
	
	public static Programmeur getProgrammeur(String login) {
		return listProgrammeur.get(login);
	}
	
	/**
	 * renvoie true si l'identification est un succès, sinon false
	 */
	public static boolean identification(String login, String motDePasse) throws NullPointerException {
		Programmeur p = listProgrammeur.get(login);
		return p.checkPassword(motDePasse);
	}
	
	/**
	 * donne une liste contenant l'ensemble des services installés
	 */
	public static List<Class<?>> getAllServices(){
		List<Class<?>> list = new LinkedList<Class<?>>();
		
		for(Map.Entry<String, Programmeur> entry : listProgrammeur.entrySet()) {
			Programmeur programmeur = entry.getValue();
			List<Class<?>> listprog = programmeur.getStartedService();
			list.addAll(listprog);
		}
		return list;
	}
	
	/**
	 * ajout un programmeur à la liste des programmeurs
	 */
	public static void ajouterProgrammeur(Programmeur p) {
		listProgrammeur.put(p.getLogin(),p);
	}
	
	/**
	 * initialise la liste des programmeurs
	 */
	private static void init() {
		Programmeur programmeur = new Programmeur("examples", "123");
		Programmeur programmeur2 = new Programmeur("Gauthier", "yes");
		try {
			programmeur.changeUrl("ftp://localhost:2121/classes/");
			programmeur2.changeUrl("ftp://localhost:2121/classes/");
		} catch (Exception e) {
			System.out.println("Erreur ftp");
		}
		listProgrammeur.put(programmeur.getLogin(), programmeur);
		listProgrammeur.put(programmeur2.getLogin(), programmeur2);
	}
	
	/**
	 * revoit true si un programmeur a déjà ce login, sinon false
	 */
	public static boolean loginAllreadyTake(String login) {
		return listProgrammeur.containsKey(login);
	}

}
