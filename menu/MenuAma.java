package menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.List;

import bri.Service;
import bri.ServiceRegistry;

public class MenuAma implements Runnable {
	
private Socket s;
	
	public MenuAma(Socket s) {
		this.s = s;
	}

	@Override
	public void run() {
		BufferedReader socketIn = null;
		PrintWriter socketOut = null;
		try {
			socketIn = new BufferedReader (new InputStreamReader(s.getInputStream ()));
			socketOut = new PrintWriter (s.getOutputStream (), true);
			
			List<Class<?>> listService = ServiceRegistry.getAllServices();
			if(listService.size()!=0) {
				String affichage = "Which service do you want ?##";
				for(int i = 0; i < listService.size(); i++) {
				Method m = listService.get(i).getMethod("toStringue");
					try {
						affichage+=(i+1)+"- " + m.invoke(listService.get(i)) + "##";
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				socketOut.println(affichage);
				int number = (Integer.parseInt(socketIn.readLine()) -1);
				try {
					Runnable r = (Runnable) listService.get(number).getConstructor(Socket.class).newInstance(s);
					r.run();
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
			} else {
				socketOut.println("There is no current service avaliable, try later");
				socketIn.readLine();
			}
		} catch(IOException e) {
			
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			} catch (IOException e) {
			}
		}
	}
	
	@Override
	protected void finalize() throws IOException {
		s.close();
	}

}
