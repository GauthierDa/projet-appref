package menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import Exception.ServiceMalForm�;
import Utilisateur.Programmeur;
import bri.ServiceRegistry;

public class MenuProg implements Runnable {
	private final Socket s;

	public MenuProg(Socket s) {
		this.s = s;
	}

	@Override
	public void run() {
		BufferedReader socketIn = null;
		PrintWriter socketOut = null;
		try {
			socketIn = new BufferedReader(new InputStreamReader(s.getInputStream()));
			socketOut = new PrintWriter(s.getOutputStream(), true);

			socketOut.println("Welcome to the server of Gauthier ##1-Login##2-Create an account");
			Programmeur p = null;
			switch (socketIn.readLine()) {
			case "1":
				p = login(socketIn, socketOut);
				break;
			case "2":
				p = createAccount(socketIn, socketOut);
				break;
			}

			boolean stay = true;
			while (stay) {
				socketOut.println("What do you want to do ?##" + "-0 See all my services##" + "-1 Add a service##"
						+ "-2 Update a service##" + "-3 Change FTP's adress##" + "-4 Start a service##"
						+ "-5 Stop a service##" + "-6 Unstall a service##" + "-7 Exit");
				try {
					int choix = Integer.parseInt(socketIn.readLine());
					switch (choix) {
					case 0:
						seeAllService(socketIn, socketOut, p);
						break;
					case 1:
						addService(socketIn, socketOut, p);
						break;
					case 2:
						updateService(socketIn, socketOut, p);
						break;
					case 3:
						changeFTPAdress(socketIn, socketOut, p);
						;
						break;
					case 4:
						startService(socketIn, socketOut, p);
						break;
					case 5:
						stopService(socketIn, socketOut, p);
						break;
					case 6:
						unstallService(socketIn, socketOut, p);
						break;
					case 7:
						stay = false;
						socketOut.println("Bye");
						break;
					default:
						socketOut.println("Not understant");
					}
				} catch (NumberFormatException e) {
					socketOut.println("This is not a choice");
				}
				socketIn.readLine();
			}

		} catch (NullPointerException e) {
			socketOut.println("Unknow login");
		} catch (IOException e) {
		}

		finally {
			try {
				s.close();
			} catch (IOException e) {
			}
		}
	}

	/**
	 * Montre la liste des services li� au programmeur
	 */
	private void seeAllService(BufferedReader in, PrintWriter out, Programmeur p) throws IOException {
		String listService = "";
		List<Class<?>> list = p.getAllServices();
		if (list.size() > 0) {
			listService = "List of your services : ##";
			for (Class<?> service : list) {
				listService += service.getSimpleName() + ":" + p.getStatusService(service) + "##";
			}
		} else {
			listService = "You do not have any service";
		}
		out.println(listService);
	}

	/**
	 * ajoute un service si ce dernier correspond � la norme BRi
	 */
	private void addService(BufferedReader in, PrintWriter out, Programmeur p) throws IOException {
		out.println("What is the name of the class ?");
		try {
			p.ajouterService(in.readLine());
			out.println("The service is now installed");
		} catch (ServiceMalForm� e) {
			out.println(e.toString() + ".##Le service ne s'est donc pas install�");
		}
	}


	/**
	 * Met � jour un service
	 */
	private void updateService(BufferedReader in, PrintWriter out, Programmeur p) throws IOException {
		List<Class<?>> listService = p.getAllServices();
		if (listService.size() == 0) {
			out.println("You have no service to update");
			return;
		}
		String s = "Which one do you want to update ?##";
		int cpt = 1;
		for (Class<?> c : listService) {
			s += cpt + "- " + c.getSimpleName() + "##";
		}
		out.println(s);
		int number = Integer.parseInt(in.readLine());
		try {
			p.updateService(number - 1);
			out.println("The service has been updated");
		} catch (ClassNotFoundException e) {
			out.println("The service is not on your ftp server");
		}

	}


	/**
	 * Change l'adresse FTP du programmeur
	 */
	private void changeFTPAdress(BufferedReader in, PrintWriter out, Programmeur p) throws IOException {
		out.println("Your current ftp's adress is " + p.getUrl() + "##What is the new one ?");

		String url = in.readLine();
		try {
			p.changeUrl(url);
			out.println("The adress has been updated");
		} catch (Exception e) {
			out.println("The adress is incorrect");
		}
	}


	/**
	 * D�marre un service install�
	 */
	private void startService(BufferedReader in, PrintWriter out, Programmeur p) throws IOException {
		List<Class<?>> listService = p.getStopedService();
		if (listService.size() == 0) {
			out.println("You have no service to start");
			return;
		}
		String s = "Which one do you want to start ?##";
		int cpt = 1;
		for (Class<?> c : listService) {
			s += cpt + "- " + c.getSimpleName() + "##";
		}
		out.println(s);
		int number = Integer.parseInt(in.readLine());
		p.startService(listService.get(number - 1));
		out.println("The service is now available");
	}


	/**
	 * Arr�te un service install�
	 */
	private void stopService(BufferedReader in, PrintWriter out, Programmeur p) throws IOException {
		List<Class<?>> listService = p.getStartedService();
		if (listService.size() == 0) {
			out.println("You have no service to stop");
			return;
		}
		String s = "Which one do you want to close ?##";
		int cpt = 1;
		for (Class<?> c : listService) {
			s += cpt + "- " + c.getSimpleName() + "##";
		}
		out.println(s);
		int number = Integer.parseInt(in.readLine());
		p.stopService(listService.get(number - 1));
		out.println("The service is now closed");
	}


	/**
	 * Supprime un service
	 */
	private void unstallService(BufferedReader in, PrintWriter out, Programmeur p) throws IOException {
		List<Class<?>> listService = p.getStartedService();
		if (listService.size() == 0) {
			out.println("You have no service to unstall");
			return;
		}
		String s = "Which one do you want to unstall ?##";
		int cpt = 1;
		for (Class<?> c : listService) {
			s += cpt + "- " + c.getSimpleName() + "##";
		}
		out.println(s);
		int number = Integer.parseInt(in.readLine());
		p.unstallService(number - 1);
		out.println("The service is now unstalled");
	}


	/**
	 * Creer un compte de programmeur
	 */
	private Programmeur createAccount(BufferedReader socketIn, PrintWriter socketOut) throws IOException {
		String error = "";
		String login;
		boolean continuer = true;
		do {
			socketOut.println(error + "Choose a login");
			login = socketIn.readLine();
			if (!ServiceRegistry.loginAllreadyTake(login))
				continuer = false;
		} while (continuer);
		continuer = false;
		socketOut.println("Choose a password");
		String motDePasse = socketIn.readLine();
		Programmeur p = new Programmeur(login, motDePasse);
		ServiceRegistry.ajouterProgrammeur(p);
		socketOut.println("Type our ftp server adress");
		while (true) {
			String ftpAdress = socketIn.readLine();
			try {
				p.changeUrl(ftpAdress);
				return p; 
			} catch (Exception e) {
				socketOut.println("The ftp adress is incorrect");
			}
		}
	}


	/**
	 * identifie un programmeur
	 */
	private Programmeur login(BufferedReader socketIn, PrintWriter socketOut) throws IOException {
		String error = "";
		do {
			socketOut.println(error + "LOGIN : ");
			String login = socketIn.readLine();
			socketOut.println("PASSWORD : ");
			String motDePasse = socketIn.readLine();
			if (ServiceRegistry.identification(login, motDePasse)) {
				return ServiceRegistry.getProgrammeur(login);
			}
			error = "Erreur de mot de passe ou de login##";
		} while (true);
	}

	@Override
	protected void finalize() throws IOException {
		s.close();
	}
}
