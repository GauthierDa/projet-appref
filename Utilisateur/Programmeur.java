package Utilisateur;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import Exception.ServiceMalForm�;
import bri.ServiceRegistry;

public class Programmeur {
	private final static String algorithm = "MD2";
	private final static String ftpURL = "^ftp://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
	private String url;
	private String login;
	private byte[] motDePasse;
	//contient la liste des services du programmeur
	private List<Class<?>> listeServices = new Vector<Class<?>>();
	//contient l'�tat de chaque service (true si active, sinon false)
	private Map<Class<?>, Boolean> etatService = new ConcurrentHashMap<Class<?>, Boolean>();

	public Programmeur(String login, String motDePasse) {
		this.login = login;
		try {
			//hachage du mot de passe
			this.motDePasse = MessageDigest.getInstance(algorithm).digest(motDePasse.getBytes());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	public String getUrl() {
		return this.url;
	}

	public void changeUrl(String url) throws Exception {
		if (url.matches(ftpURL))
			this.url = url;
		else
			throw new Exception();
	}

	public boolean checkPassword(String password) {
		try {
			MessageDigest hash = MessageDigest.getInstance(algorithm);
			byte[] hashedPassword = hash.digest(password.getBytes());
			return MessageDigest.isEqual(this.motDePasse, hashedPassword);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return false;
	}

	public String getLogin() {
		return this.login;
	}

	/**
	 * ajouter un service au programmeur
	 * @param nomService nom du Service stock� dans le ftp du programmeur
	 * @throws ClassNotFoundException le nomService n'est pas correct
	 * @throws MalformedURLException le ftp est injoignable
	 * @throws ServiceMalForm� le service ne respecte pas les r�gles de structure des services
	 */
	public void ajouterService(String nomService) throws ServiceMalForm� {
		try {
		@SuppressWarnings("resource")
		URLClassLoader u = new URLClassLoader(new URL[] {new URL(this.url)});
		Class<?> service = u.loadClass(nomService);
		String nomPackage = service.getPackage().getName();
		if(nomPackage.equals(this.login.toLowerCase())) {
			ServiceRegistry.addService(service);
			listeServices.add(service);
			etatService.put(service, true);
		} else {
			throw new ServiceMalForm�("Le package ne correspond pas � la personne");
		}
			Socket s = new Socket();
			service.getConstructor(Socket.class).newInstance(s);
			s.close();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			throw new ServiceMalForm�("Le service n'arriva pas � se lancer");
		}catch(	ClassNotFoundException e1 ){
			throw new ServiceMalForm�("Le nom du service est mal orthographi�");
		} catch (IOException e) {
			// Normalement on passe jamais ici
			throw new ServiceMalForm�("Socket ferm�e");
		}
	}

	/**
	 * Renvoie la liste des services en fonction de l'�tat demand�
	 * 
	 * @param etat boolean qui d�crit l'�tat des service demand� (true pour actif,
	 *             false pour arr�t�)
	 * @return la liste des services du programmeur
	 */
	private List<Class<?>> getServices(Boolean etat) {
		List<Class<?>> retour = new LinkedList<Class<?>>();
		for (Class<?> service : listeServices) {
			if (etatService.get(service) == etat)
				retour.add(service);
		}
		return retour;
	}

	public void updateService(int number) throws ClassNotFoundException, MalformedURLException {
		//r�cup�re le service � metrte � jour
		Class<?> service = listeServices.get(number);
		//supprimer le service et son �tat
		listeServices.remove(number);
		boolean etat = etatService.get(service);
		etatService.remove(service);
		String nameService = service.getPackage().getName() + "." + service.getSimpleName();
		//r�cuperer le nouveau service
		@SuppressWarnings("resource")
		URLClassLoader u = new URLClassLoader(new URL[] { new URL(this.url) });
		service = u.loadClass(nameService);
		//ajouter le nouveau service � la m�me place que l'ancien
		listeServices.add(service);
		etatService.put(service, etat);
	}

	/**
	 * donne la liste des services qui sont install�s et d�marr�s
	 */
	public List<Class<?>> getStartedService() {
		return getServices(true);
	}

	/**
	 * donne la liste des services qui sont install�s et arr�t�s
	 */
	public List<Class<?>> getStopedService() {
		return getServices(false);
	}

	/**
	 * D�mare le service pass� en param�tre
	 */
	public void startService(Class<?> service) {
		etatService.put(service, true);
	}

	/**
	 * Arr�te le service pass� en param�tre
	 */
	public void stopService(Class<?> service) {
		etatService.put(service, false);
	}

	/**
	 * supprime un service
	 * @param le num�ro du service
	 */
	public void unstallService(int number) {
		Class<?> service = listeServices.get(number);
		etatService.remove(service);
		listeServices.remove(number);
	}

	/**
	 * donne la liste des services du programmeur
	 */
	public List<Class<?>> getAllServices() {
		return listeServices;
	}

	/**
	 * donne le status du service pass� en param�tre
	 * @return Active si activ�, sinon Closed
	 */
	public String getStatusService(Class<?> service) {
		if (etatService.get(service))
			return "Active";
		return "Closed";
	}
}
