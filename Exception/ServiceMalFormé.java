package Exception;

public class ServiceMalFormé extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String raison;

	public ServiceMalFormé(String raison) {
		this.raison = raison;
	}
	
	public String toString() {
		return this.raison;
	}

}
