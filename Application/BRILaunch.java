package Application;

import java.io.IOException;

import bri.Serveur;
import menu.MenuAma;
import menu.MenuProg;

public class BRILaunch {
	private final static int PORT_PROG = 2000 ;
	private final static int PORT_AMAT = 2100;
	
	
	public static void main(String args[]) {
		try {
			new Thread(new Serveur(PORT_PROG, MenuProg.class)).start();
			System.out.println("Lancement du serveur MenuProg");
			new Thread(new Serveur(PORT_AMAT, MenuAma.class)).start();
			System.out.println("Lancement du serveur MenuAma");
		} catch (IOException e) {
			System.out.println("Erreur de lancement d'un des serveurs");
		}	
	}
}
